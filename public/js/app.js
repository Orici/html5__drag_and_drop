
/**
 * Global vars
 *
 */

var dragSrcEl = null;


/**
 * Function declarations
 *
 */

function handleDragOver(event)
{
    if (event.preventDefault) {
        event.preventDefault(); // Necessary to allow us to drop
    }

    event.dataTransfer.dropEffect = 'move';

    return false;
}

function handleDragStart(event)
{
    // Target (this) element is the source nodevent.
    dragSrcEl = this;

    event.dataTransfer.effectAllowed = 'move';
    event.dataTransfer.setData('text/html', this.innerHTML);

    this.classList.add('moving');
}

function handleDrop(event)
{
    if (event.stopPropagation) {
        event.stopPropagation(); // Stops some browsers' redirection
    }

    // Don't do anything if dropping the same space we're dragging
    if (dragSrcEl != this) {
        dragSrcEl.classList.remove('moving');

        // Set the source item's HTML to the HTML of the item dropped on
        dragSrcEl.innerHTML = this.innerHTML;
        this.innerHTML = event.dataTransfer.getData('text/html');
    }

    return false;
}


/**
 * Event listeners
 *
 */

var draggable_items = document.querySelectorAll('.cards-box .card');
[].forEach.call(draggable_items, function (draggable_item) {
    draggable_item.addEventListener('dragover',  handleDragOver,  false);
    draggable_item.addEventListener('dragstart', handleDragStart, false);
    draggable_item.addEventListener('drop',      handleDrop,      false);
});

// NOTE:  inside the handle event functions this or event.target is the
// current moved node


/**
 * App's code
 *
 */
